

function drawChart(){
  d3.select("svg").remove();
  //Setup of size of svg
  var margin = {top: 20, right: 200, bottom: 100, left: 50},
      margin2 = { top: 430, right: 10, bottom: 20, left: 40 },
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom,
      height2 = 500 - margin2.top - margin2.bottom;


  // Parsing date in data
  var parseDate = d3.time.format("%Y").parse;

  var bisectDate = d3.bisector(function(d) { return d.date; }).left;

  //scale for chart
  var xScale = d3.time.scale()
      .range([0, width]);

  //second scale for brush
  var xScale2 = d3.time.scale()
      .range([0, width]);

  var yScale = d3.scale.pow().exponent(0.33)
      .range([height, 0]);
  var maxY;



  // Color codes
  var color = d3.scale.ordinal().range(["#CC0000","#CC3300","#CC6600","#CC9900","#CCCC00","#CCFF00","#FF0000","#FF3300","#FF6600","#FF9900","#FFCC00","#FFFF00","#990000","#993300","#996600","#999900","#99CC00","#33FF00","#660000","#660033","#666600","#669900","#CC0033","#CC3333","#CC6633","#CC9933","#CCCC33","#CCFF33","#FF0033","#FF3333","#FF6633","#FF9933","#FFCC33","#FFFF33","#990033","#993333","#996633","#999933","#99CC33","#33FF33","#660033","#663333","#666633","#669933","#CC0066","#CC3366","#CC6666","#CC9966","#CCCC66","#CCFF66","#FF0066","#FF3366","#FF6666","#FF9966","#FFCC66","#FFFF66","#990066","#993366","#996666","#999966","#99CC66","#33FF66","#660066","#663366","#666666","#669966","#CC0099","#CC3399","#CC6699","#CC9999","#CCCC99","#CCFF99","#FF0099","#FF3399","#FF6699","#FF9999","#FFCC99","#FFFF99","#990099","#993399","#996699","#999999","#99CC99","#33FF99","#660099","#663399","#666699","#669999","#CC00CC","#CC33CC","#CC66CC","#CC99CC","#CCCCCC","#CCFFCC","#FF00CC","#FF33CC","#FF66CC","#FF99CC","#FFCCCC","#FFFFCC","#9900CC","#9933CC","#9966CC","#9999CC","#99CCCC","#33FFCC","#6600CC","#6633CC","#6666CC","#6699CC","#CC00FF","#CC33FF","#CC66FF","#CC99FF","#CCCCFF","#CCFFFF","#FF00FF","#FF33FF","#FF66FF","#FF99FF","#FFCCFF","#FFFFFF","#9900FF","#9933FF","#9966FF","#9999FF","#99CCFF","#33FFFF","#6600FF","#6633FF","#6666FF","#6699FF",]);


  var xAxis = d3.svg.axis()
      .scale(xScale)
      .orient("bottom"),

      xAxis2 = d3.svg.axis() // xAxis for brush slider
      .scale(xScale2)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(yScale)
      .orient("left");

  var line = d3.svg.line()
      .interpolate("basis")
      .x(function(d) { return xScale(d.date); })
      .y(function(d) { return yScale(d.gdp); })
      // .defined(function(d) { return d.gdp; });

  var svg = d3.select("#chart").append("svg").attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom).append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  var focus = svg.append("g").style("display","none");
  // Create invisible rect for mouse tracking
  svg.append("rect")
      .attr("width", width)
      .attr("height", height)
      .attr("x", 0)
      .attr("y", 0)
      .attr("id", "mouse-tracker")
      .style("fill", "white");

  // Brushing context box container
  var context = svg.append("g")
      .attr("transform", "translate(" + 0 + "," + 410 + ")")
      .attr("class", "context");

  svg.append("defs")
      .append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", width)
      .attr("height", height);
  // Data import and callback function
  d3.tsv("gdp.tsv", function(error, data) {
    color.domain(d3.keys(data[0]).filter(function(key) {return key !== "date";}));

    // Creating JS date objects for all years
    data.forEach(function(d) {
      d.date = parseDate(d.date);
    });

    var data2 = color.domain().map(function(name) {
      return {
        name: name,
        values: data.map(function(d) {
          return {
            date: d.date,
            gdp: +(d[name]),
            };
        }),
        visible: (name === "India" || name === "Pakistan" || name === "China"? true : false) // "visible": all false except for economy which is true.
      };
    });

    xScale.domain(d3.extent(data, function(d) { return d.date; }));

    yScale.domain([0, d3.max(data2, function(c) { return d3.max(c.values, function(v) { return v.gdp; }); })
    ]);
    maxY = yMaximum(data2);
    yScale.domain([0,maxY]);
    xScale2.domain(xScale.domain());

  // Creating brush slider
   var brush = d3.svg.brush().x(xScale2).on("brush", brushed);

    context.append("g")
      .attr("class", "x axis1")
      .attr("transform", "translate(0," + height2 + ")")
      .call(xAxis2);

    var contextArea = d3.svg.area()
      .interpolate("monotone")
      .x(function(d) { return xScale2(d.date); })
      .y0(height2)
      .y1(0);

    //plot the rect as the bar at the bottom
    context.append("path") // Path is created using svg.area details
      .attr("class", "area")
      .attr("d", contextArea(data2[0].values)) // pass first data2 data .values to area path generator
      .attr("fill", "#F1F1F2");

    // Append the brush for the selection of subsection
    context.append("g")
      .attr("class", "x brush")
      .call(brush)
      .selectAll("rect")
      .attr("height", height2)
      .attr("fill", "#E6E7E8");

    // draw line graph
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("x", -10)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text("GDP, In Billions");

    var legend = svg.selectAll(".legend")
        .data(data2)
        .enter().append("g")
        .attr("class", "legend");

    legend.append("path")
        .attr("class", "line")
        .style("pointer-events", "none")
        .attr("id", function(d) {
          return "line-" + d.name.replace(" ", "").replace("/", "");
        })
        .attr("d", function(d) {
          return d.visible ? line(d.values) : null;
        })
        .attr("clip-path", "url(#clip)")
        .style("stroke", function(d) { return color(d.name); });

    var legendHeight = 600 / data2.length;

    legend.append("rect")
        .attr("width", 10)
        .attr("height", 10)
        .attr("x", width + (margin.right/3) - 15)
        .attr("y", function (d, i) { return (legendHeight)+i*(legendHeight) - 8; })
        .attr("fill",function(d) {
          return d.visible ? color(d.name) : "#F1F1F2";
        })
        .attr("class", "legend-box")

        .on("click", function(d){
          d.visible = !d.visible;
          maxY = yMaximum(data2);
          yScale.domain([0,maxY]);
          svg.select(".y.axis")
            .transition()
            .call(yAxis);

        legend.select("path")
            .transition()
            .attr("d", function(d){
              return d.visible ? line(d.values) : null;
            })

        legend.select("rect")
            .transition()
            .attr("fill", function(d) {
            return d.visible ? color(d.name) : "#F1F1F2";
          });
        })
        .on("mouseover", function(d){
          d3.select(this)
            .transition()
            .attr("fill", function(d) { return color(d.name); });

          d3.select("#line-" + d.name.replace(" ", "").replace("/", ""))
            .transition()
            .style("stroke-width", 2.5);
        })
        .on("mouseout", function(d){
          d3.select(this)
            .transition()
            .attr("fill", function(d) {
            return d.visible ? color(d.name) : "#F1F1F2";});

          d3.select("#line-" + d.name.replace(" ", "").replace("/", ""))
            .transition()
            .style("stroke-width", 1.5);
        })

    legend.append("text")
        .attr("x", width + (margin.right/3))
        .attr("y", function (d, i) { return (legendHeight)+i*(legendHeight); })
        .text(function(d) { return d.name; });

    // svg.append("circle")
    //      .attr("class", "logo")
    //      .attr("cx", 225)
    //      .attr("cy", 225)
    //      .attr("r", 20)
    //      .on("mouseover", function(d){
    //          tooltip.text("some text");
    //          tooltip.append("img")
    //                  .attr("src","https://github.com/favicon.ico")
    //                  .attr("x", -8)
    //                  .attr("y", -8)
    //                  .attr("width","16px")
    //                  .attr("height","12px");
    //          tooltip.style("visibility", "visible");
    //      })
    //      .on("mousemove", function(){return tooltip.style("top", (event.pageY-
    //                                    10)+"px").style("left",(event.pageX+10)+"px");})
    //      .on("mouseout", function(){return tooltip.style("visibility", "hidden");});


    var mouseG = svg.append("g")
        .attr("class", "mouse-over-effects");

      mouseG.append("path")
        .attr("class", "mouse-line")
        .style("stroke", "black")
        .style("stroke-width", "1px")
        .style("opacity", "0");

      var lines = document.getElementsByClassName('line');

      var mousePerLine = mouseG.selectAll('.mouse-per-line')
        .data(data2)
        .enter()
        .append("g")
        .attr("class", "mouse-per-line");

      mousePerLine.append("circle")
        .attr("r", 3)
        .style("stroke", function(d) {return color(d.name);})
        .style("fill", function(d) {return color(d.name);})
        .style("stroke-width", "1px")
        .style("opacity", "0");

      mousePerLine.append("text")
        .attr("transform", "translate(10,3)");

        mouseG.append('svg:rect')
          .attr('width', width)
          .attr('height', height)
          .attr('fill', 'none')
          .attr('pointer-events', 'all')
          .on('mouseout', function() {
            d3.select(".mouse-line")
              .style("opacity", "0");
            d3.selectAll(".mouse-per-line circle")
              .style("opacity", "0");
            d3.selectAll(".mouse-per-line text")
              .style("opacity", "0");
          })
          .on('mouseover', function() { // on mouse in show line, circles and text
            d3.select(".mouse-line")
              .style("opacity", "1");
            d3.selectAll(".mouse-per-line circle")
              .style("opacity", "1");
            d3.selectAll(".mouse-per-line text")
              .style("opacity", "1");
          })
          .on('mousemove', function() { // mouse moving over canvas
            var mouse = d3.mouse(this);
            d3.select(".mouse-line")
              .attr("d", function() {
                var d = "M" + mouse[0] + "," + height;
                d += " " + mouse[0] + "," + 0;
                return d;
              });
        d3.selectAll(".mouse-per-line")
            .attr("transform", function(d, i) {
              var xDate = xScale.invert(mouse[0]),
                  bisect = d3.bisector(function(d) { return d.date; }).right;
                  idx = bisect(d.values, xDate);
                  // console.log(idx);
                  // console.log(xDate);
                  // console.log(i);
              var beginning = 0,
                  end = lines[i].getTotalLength(),
                  target = null;
                  // console.log(lines[i].getTotalLength());

              while (true){
                target = Math.floor((beginning + end) / 2);
                // console.log(target);
                pos = lines[i].getPointAtLength(target);
                if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                    break;
                }
                if (pos.x > mouse[0])      end = target;
                else if (pos.x < mouse[0]) beginning = target;
                else break; //position found
              }

              d3.select(this).select('text')
                .text(yScale.invert(pos.y).toFixed(2));

              return "translate(" + mouse[0] + "," + pos.y +")";
            });
        });

    //for brusher of the slider bar at the bottom
    function brushed() {
      xScale.domain(brush.empty() ? xScale2.domain() : brush.extent());
      svg.select(".x.axis").transition().call(xAxis);
      maxY = yMaximum(data2);
      yScale.domain([0,maxY]);
      svg.select(".y.axis").transition().call(yAxis);
      legend.select("path").transition().attr("d", function(d){
          return d.visible ? line(d.values) : null;
        });

    };

  });
}
// Calculates highest values of y
  function yMaximum(data){
    var maxYValues = data.map(function(d) {
      if (d.visible){
        return d3.max(d.values, function(value) {
          return value.gdp; })
      }
    });
    return d3.max(maxYValues);
  }
drawChart()
